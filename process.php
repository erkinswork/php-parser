<?php
ini_set('max_execution_time', 300);
include_once('vendor/autoload.php');
use DiDom\Document;
use DiDom\Query;

$cookie_file = "cookies.txt";
$url = $_POST['url'];
$url = $url.'/?PAGEN_1='.$_POST['curr_page'];

$ch = curl_init();
curl_setopt($ch, CURLOPT_URL, $url);
curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
curl_setopt($ch, CURLOPT_USERAGENT, "Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.8.1.3) Gecko/20070309 Firefox/2.0.0.3");
curl_setopt($ch, CURLOPT_COOKIEJAR, $cookie_file);
curl_setopt($ch, CURLOPT_COOKIEFILE, $cookie_file);
curl_setopt($ch, CURLOPT_REFERER, $url);
curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
$data = curl_exec($ch);

// print_r($data);

$document = new Document($data);
$divs = $document->find("//div[contains(@class, 'catalog-section')]", Query::TYPE_XPATH);
$div_html = $divs[0]->innerHtml();

echo json_encode(['html' => $div_html,'status' => 1]);