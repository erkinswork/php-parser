<?php

$url = $_POST['url'];
$temp_arr =  explode("/", $url);
$file_name = end($temp_arr);


$product_array = $_POST['product_array'];

$file_path = 'output/'.$file_name.'.csv';

if(file_exists($file_path)) 
{ 
	unlink($file_path);
}

$fp = fopen($file_path, "w+");
fputcsv($fp, ['name','price','url','image']);
foreach ($product_array as $fields) {
    fputcsv($fp, array_values($fields));
}
fclose($fp);

echo json_encode(['file_name' => $file_name.'.csv is saved!','status' => 1,'path' => $file_path]);