var curr_page = 1;
var product_array = [];
var product_id_array = [];
var is_last_page = 0;

function startUrl(url,curr_page)
{
    $('.statusTxt h3').text('Сканирование... Пожалуйста подаждите!');
    $('.statusTxt').show();

    // Отправляем данные в process.php (Curl), где мы получаем данные из страниц сайта, декодируем данные в json формат
    // запускаем библиотеку Имана-Газалиева (HTML_PARSER COMPOSER) которая парсит данные по переданным ему настройкам.
    $.ajax({
        url: "process.php",
        method: 'post',
        dataType:'json',
        data:{'url':url,'curr_page':curr_page},
        success: function(resp){
            var this_html = resp.html;
            var base_url = 'http://www.mnogomarok.ru';
            $(this_html).find('.products__item').each(function(key,this_div){
                var prod_obj = {};
                var product_url = base_url+$(this_div).find('.products__item-pic img').attr('src');
                var product_link = base_url+$(this_div).find('.products__item-title a').attr('href');
                var product_name = $(this_div).find('.products__item-title a').text();
                var product_price = $(this_div).find('.products__item-price .pr').text();
                prod_obj['name'] = product_name.trim();
                prod_obj['link'] = product_link;
                prod_obj['price'] = product_price;
                prod_obj['image_url'] = product_url;
                var link_arr = product_link.split("/");
                if($.inArray(link_arr[4], product_id_array) == -1)
                {
                    product_id_array.push(link_arr[4]);
                    product_array.push(prod_obj);
                }
                else
                {
                    is_last_page = 1;
                    return false;
                }
            });
            if(is_last_page == 0)
            {
                $('.statusCount').text(curr_page);
                curr_page = curr_page + 1;
                startUrl(url,curr_page);
            }
            else
            {
                // После сканирования всех страниц, отправляем данные в save.php, где мы, открываем доступ к файловой
                // системе и записываем все данные полученные из HTML парсера в csv файл в папку /output. Конечно можно
                // было бы и подключить библиотеку, которая работает с файловой системой, но для этой задачи думаю
                // покамесь достаточно.
                $.ajax({
                    url: "save.php",
                    method: 'post',
                    dataType:'json',
                    data:{'url':url,'product_array':product_array},
                    success: function(resp){
                        if(resp.status == 1)
                        {
                            $('.downBtn').attr('href',resp.path).show();
                            $('.statusTxt h3').text('Сканирование завершено!.');
                        }
                    }
                });
            }
        },error: function(data){}
    });
}

// Запускаем тригер при нажатии кнопки
$(document).on('click','.startBtn',function(){
    var url_inp = $('.categoryInp').val();
    startUrl(url_inp,curr_page);
});

