<!doctype html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<title>Demo App</title>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" href="css/bootstrap.min.css">
	<link rel="stylesheet" href="css/style.css">
	<script src="js/jquery.min.js"></script>
</head>

<body>
      <div class="container text-center parser-custom">
      	<h3>Php Парсер "Марки и Подарки"</h3>
      	<input type="text" class="form-control categoryInp" placeholder="Пожалуйста введите ссылку" value="http://www.mnogomarok.ru/catalog/nepochtovye-marki" disabled>
      	<br>
      	<button class="startBtn btn btn-primary">Начать</button>
      </div>
      <div class="statusTxt" style="display: none;text-align: center;">
      	<h3 class="text-center"></h3>
      	<div class="text-center"> Просканированно <b><span class="statusCount">0</span></b> страниц.</div>
        <a class="btn btn-success btn-large downBtn" style="display: none;" href="" target="_blank" download>Скачать</a>
      </div>
	  <script src="js/main.js"></script>
</body>
</html>
